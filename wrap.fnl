(local lovely-levels (require :lovely-levels))
(local assets (require :lovely-assets))
(local tile-width 16)

(local map {})

(fn love.load []
  (love.graphics.setDefaultFilter :nearest :nearest)
  (tset map :parsed-map (lovely-levels.parse-map lovely-levels.demo-map))
  (tset map :quad-list (lovely-levels.make-quad-list-4x4 tile-width 0 0 128 128))
  (assets.add :assets/atlas.png))

(fn love.draw []
  (love.graphics.scale 8)
  ;;(love.graphics.draw (. assets :assets :atlas.png))
  (lovely-levels.nprint-map 0 0 (. assets :assets :assets :atlas.png)
                            map.quad-list map.parsed-map tile-width)
  )

(fn love.update [dt]
  (assets.update dt)
  ;;(pp assets.update-queue)
  )

(pp (love.filesystem.getInfo "assets/atlas.png"))
