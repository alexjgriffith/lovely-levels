(macro incf [x] `(do (set ,x (+ ,x 1)) ,x))
(macro incfb [x b] `(do (set ,x (+ ,x 1)) (when (> ,x ,b) (set ,x 1)) ,x))
(macro set1 [x] `(do (set ,x 1) ,x))

(local demo-map
       "
CCCCCCCCCCCC
CCCCCCCCCCCC
CCCCCCCCCCCC
CCD330CCCCCC
CC899FCCCCCC
CCCCCCCCCCCC
CCCCCCCCCCCC
CCCCCCCCCCCC
CCCCCCCCCCCC
")

(fn parse-map [map]
  (fn hex-to-dec [value]
    (?. {:0 0 :1 1 :2 2 :3 3 :4 4 :5 5 :6 6 :7 7 :8 8
         :9 9 :A 10 :B 11 :C 12 :D 13 :E 14 :F 15}
        value))
  (local parsed {})
  (var col-number 1)
  (var row-number 1)
  (each [row (string.gmatch map "(%w+)" )]
    (tset parsed row-number {})
    (each [value (string.gmatch row ".")]
      (tset parsed row-number col-number (+ 1 (hex-to-dec value)))
      (incf col-number))
    (set1 col-number)
    (incf row-number))
  parsed)

(fn make-quad-list-4x4 [px x? y? iw? ih?]
  (let [w (* px 4) h (* px 4)
        x (or x? 0) y (or y? 0)
        iw (or ih? h) ih (or iw? w)
        tab {}]
    (for [row 1 4]
      (for [col 1 4]
        (let [xo (+ x (* px (- col 1)))
              yo (+ x (* px (- row 1)))]
          (table.insert tab (love.graphics.newQuad xo yo px px iw ih)))))
    tab))

(fn nprint-map [x y image quad-list parsed-map px]
  (local lg love.graphics)
  (lg.push)
  (lg.translate x y)
  (each [row-number row (ipairs parsed-map)]
    (each [col-number value (ipairs row)]
      (let [quad (. quad-list value)
            xo (* px (- col-number 1))
            yo (* px (- row-number 1))]
        (lg.draw image quad xo yo))))
  (lg.pop))


{: demo-map
 : parse-map
 : make-quad-list-4x4
 : nprint-map}
